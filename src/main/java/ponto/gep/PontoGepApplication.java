package ponto.gep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import ponto.gep.crawler.ahgora.AhgoraBatidaOnline;
import ponto.gep.service.PreenchePlanilhaProdespService;

@SpringBootApplication
public class PontoGepApplication implements CommandLineRunner {

	@Autowired private PreenchePlanilhaProdespService preenchePlanilhaProdespService;
	@Autowired private AhgoraBatidaOnline ahgoraBatidaOnline;

	public static void main(String[] args) throws Exception {
		new SpringApplicationBuilder(PontoGepApplication.class)
		.bannerMode(Mode.OFF)
		.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		preenchePlanilhaProdespService.preenchePlanilha();
//		ahgoraBatidaOnline.baterPonto();
		System.exit(0);
	}

}
