package ponto.gep.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ponto.gep.crawler.ahgora.AhgoraConsulta;
import ponto.gep.crawler.ahgora.AhgoraLogin;
import ponto.gep.crawler.ahgora.dto.DiaDTO;
import ponto.gep.util.ExcelUtil;

@Service
public class PreenchePlanilhaProdespService {

	@Autowired private AhgoraLogin ahgoraLogin;
	@Autowired private AhgoraConsulta ahgoraConsulta;

	@Value("${planilha.prodesp.tipo.tarefa}") private String tipoTarefa;
	@Value("${planilha.prodesp.natureza.tarefa}") private String naturezaTarefa;
	@Value("${planilha.prodesp.sistema}") private String sistema;
	@Value("${planilha.prodesp.funcionalidade}") private String funcionalidade;
	@Value("${planilha.prodesp.sm.email}") private String smEmail;
	@Value("${planilha.prodesp.tarefa}") private String tarefa;

	public void preenchePlanilha() {
		try {

			System.out.println("Realizando login");
			ahgoraLogin.login();

			System.out.println("Buscando espelho do ponto");
			List<DiaDTO> horariosDoMes = ahgoraConsulta.getHorariosDoMes(LocalDate.now());

			System.out.println("Tratando dados");
			printaDias(horariosDoMes);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void printaDias(List<DiaDTO> horariosDoMes) {
		System.out.println("----------------------------------------------------------------------------------------------------");
		horariosDoMes.forEach(dia -> System.out.print(
				ExcelUtil.escreveLinha(
						dia.getDia().toString(),
						tipoTarefa,
						naturezaTarefa,
						sistema,
						funcionalidade,
						smEmail,
						tarefa, 
						dia.getHoras())));
		System.out.println("----------------------------------------------------------------------------------------------------");
	}

}
