package ponto.gep.util;

public class ExcelUtil {

	private static final String BREAK_CELL = "	";
	private static final String BREAK_LINE = "\n";
	
	public static final String escreveLinha(String... celulas) {
		StringBuilder sb = new StringBuilder();
		for (String celula : celulas) {
			sb.append(celula == null ? "" : celula).append(BREAK_CELL);
		}
		return sb.append(BREAK_LINE).toString();
	}
	
}
