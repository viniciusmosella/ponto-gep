package ponto.gep.crawler.ahgora;

import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import ponto.gep.crawler.ahgora.web.client.config.WebClientFactory;

@Component
public class AhgoraLogin {

	private static final String URL_PAGINA_LOGIN = "https://www.ahgora.com.br/externo/index/a889949";
	private static final String XPATH_CSRF_TOKEN = ".//form[@id='boxLogin']//input[@name='csrf_token']";
	private static final String EMPRESA = "a889949";
	private static final String URL_LOGIN_POST = "https://www.ahgora.com.br/externo/login";

	@Value("${ahgora.matricula}") private String matricula;
	@Value("${ahgora.senha}") private String senha;

	public void login() throws Exception {
		HtmlPage paginaInicial = WebClientFactory.getInstance().getPage(URL_PAGINA_LOGIN);
		String csrfToken = paginaInicial.<HtmlInput>getFirstByXPath(XPATH_CSRF_TOKEN).getValueAttribute();
		final WebRequest loginRequest = new WebRequest(new URL(URL_LOGIN_POST), HttpMethod.POST);
		loginRequest.setRequestBody(new StringBuilder()
				.append("empresa=").append(EMPRESA)
				.append("&csrf_token=").append(csrfToken)
				.append("&origin=portal")
				.append("&matricula=").append(matricula)
				.append("&senha=").append(senha)
				.toString());
		Page paginaLogado = WebClientFactory.getInstance().getPage(loginRequest);
		if(paginaLogado.getWebResponse().getContentAsString().contains("incorreto")) {
			throw new RuntimeException("erro de login");
		}
	}

}
