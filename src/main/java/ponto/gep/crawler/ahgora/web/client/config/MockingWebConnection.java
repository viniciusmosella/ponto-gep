package ponto.gep.crawler.ahgora.web.client.config;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebConnection;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.WebResponseData;
import com.gargoylesoftware.htmlunit.util.FalsifyingWebConnection;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

public class MockingWebConnection extends FalsifyingWebConnection {

	private final Map<String, MockedResponse> FILTERED_URLS = new HashMap<>();
	private static final String NOSCRIPT_TAG = "<noscript>(\\W|.)*?</noscript>";
	private static final List<Charset> STANDARD_CHARSETS = Arrays.asList(
			StandardCharsets.ISO_8859_1,
			StandardCharsets.UTF_8,
			StandardCharsets.US_ASCII,
			StandardCharsets.UTF_16,
			StandardCharsets.UTF_16BE,
			StandardCharsets.UTF_16LE
			);

	public MockingWebConnection(WebConnection webConnection) throws IllegalArgumentException {
		super(webConnection);
	}

	public MockingWebConnection(WebClient webClient) throws IllegalArgumentException {
		super(webClient);
	}

	public void mockUrl(final URL url, final MockedResponse response) {
		FILTERED_URLS.put(url.toString(), response);
	}

	private WebResponse createWebResponse(final WebRequest wr, final byte[] content, final String contentType,
			final String encoding) throws IOException {
		final List<NameValuePair> headers = new ArrayList<>();
		headers.add(new NameValuePair(HttpHeaders.CONTENT_TYPE, contentType + "; charset=" + encoding));
		final WebResponseData wrd = new WebResponseData(content, 200, "OK", headers);
		return new WebResponse(wrd, wr.getUrl(), wr.getHttpMethod(), 0);
	}

	@Override
	public WebResponse getResponse(final WebRequest request) throws IOException {
		return Optional.ofNullable(FILTERED_URLS.get(request.getUrl().toString()))
				.map(mockedResponse -> {
					try {
						return createWebResponse(request, mockedResponse.getContent(), mockedResponse.getContentType(), mockedResponse.getEncoding());
					} catch (IOException e) {
						throw new UncheckedIOException(e);
					}
				})
				.orElse(this.getResponseHandlingNoScript(request,super.getResponse(request)));
	}

	private WebResponse getResponseHandlingNoScript(WebRequest request, WebResponse originalResponse) {

		try {
			if (Objects.isNull(originalResponse.getResponseHeaderValue("Content-Type")) || !originalResponse.getResponseHeaderValue("Content-Type").contains("text/html")) {
				return originalResponse;
			}

			final String responseBody = originalResponse.getContentAsString();
			final byte[] body = this.removeNoScriptTag(responseBody).getBytes(getContentCharset(originalResponse));
			final int statusCode = originalResponse.getStatusCode();
			final String statusMessage = originalResponse.getStatusMessage();
			final List<NameValuePair> responseHeaders = new ArrayList<NameValuePair>(originalResponse.getResponseHeaders());

			//remove header que informa que o content está compactado, pois não está mais.
			responseHeaders.remove(new NameValuePair(HttpHeaders.CONTENT_ENCODING, "gzip"));

			WebResponse newResponse = new WebResponse(new WebResponseData(body, statusCode, statusMessage, responseHeaders),
					request,originalResponse.getLoadTime());

			return newResponse;

		}catch(Throwable e) {

			StringBuilder message = new StringBuilder();
			message.append("Response headers[ ").append(originalResponse.getResponseHeaders().stream()
					.map(nv -> nv.getName()+":"+nv.getValue())
					.collect(Collectors.joining(",")))
			.append(" ]\n");
			message.append("Response content: ").append(originalResponse.getContentAsString()).append("\n");
			message.append("Request URL: ").append(originalResponse.getWebRequest().getUrl()).append("\n");
			message.append("Request body: ").append(originalResponse.getWebRequest().getRequestBody()).append("\n");
			message.append("Original Exception class: ").append(e.getClass().getName()).append("\n");
			message.append("Original Exception message: ").append(e.getMessage()).append("\n");

			throw new RuntimeException(message.toString(),e);
		}
	}

	private Charset getContentCharset(WebResponse response) {
		for(Charset standardCharset : STANDARD_CHARSETS) {
			if(StringUtils.containsIgnoreCase(response.getResponseHeaderValue("Content-Type"), standardCharset.toString())) {
				return standardCharset;
			}    		
		}
		return StandardCharsets.UTF_8;
	}

	private String removeNoScriptTag(final String content){
		return content.replaceAll(NOSCRIPT_TAG, StringUtils.EMPTY);
	}

	public static class MockedResponse {
		private static final String EMPTY_HTML = "<html></html>";
		private static final String UTF8 = "UTF-8";

		private byte[] content;
		private String contentType;
		private String encoding = "UTF-8";

		public MockedResponse(byte[] content, String contentType) {
			this(content, contentType, UTF8);
		}

		public MockedResponse(byte[] content, String contentType, String encoding) {
			this.content = content == null ? null : content.clone();
			this.contentType = contentType;
			this.encoding = encoding;
		}

		public static MockedResponseBuilder builder() {
			return new MockedResponseBuilder();
		}

		public static MockedResponse empty() {
			return new MockedResponse(EMPTY_HTML.getBytes(StandardCharsets.UTF_8), MediaType.TEXT_HTML_VALUE, UTF8);
		}

		public byte[] getContent() {
			return content == null ? null : content.clone();
		}

		public void setContent(byte[] content) {
			this.content = content == null ? null : content.clone();
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}

		public String getEncoding() {
			return encoding;
		}

		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}

		public static class MockedResponseBuilder {

			private byte[] content;
			private String contentType;
			private String encoding;

			public MockedResponseBuilder setContent(byte[] content) {
				this.content = content == null ? null : content.clone();
				return this;
			}

			public MockedResponseBuilder setContentType(String contentType) {
				this.contentType = contentType;
				return this;
			}

			public MockedResponseBuilder setEncoding(String encoding) {
				this.encoding = encoding;
				return this;
			}

			public MockedResponse build() {
				return new MockedResponse(this.content, this.contentType, this.encoding);
			}
		}

	}
}
