package ponto.gep.crawler.ahgora.web.client.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CollectingAlertHandler;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebConnection;


public class WebClientFactory {

    private static final int DEFAULT_REQUEST_TIMEOUT = 180000;
	private static final List<String> MOCKED_URLS = List.of();

    private static ThreadLocal<WebClient> instance = new ThreadLocal<>();

    public static WebClient getInstance() {
        if (instance.get() == null) {
            init();
        }
        return instance.get();
    }

    public static void init() {
        final WebClient webClient = new WebClient(BrowserVersion.CHROME);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        webClient.setAlertHandler(new CollectingAlertHandler());
        webClient.setWebConnection(createWebConnection(webClient));
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setGeolocationEnabled(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(true);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setRedirectEnabled(true);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setTimeout(DEFAULT_REQUEST_TIMEOUT);
        webClient.addRequestHeader("Accept-Language", "pt-br");
        instance.set(webClient);
    }
    
	private static WebConnection createWebConnection(final WebClient webClient) {
		final MockingWebConnection mockingWebConnection = new MockingWebConnection(webClient);

		MOCKED_URLS.forEach(mockedUrl -> {
			try {
				mockingWebConnection.mockUrl(new URL(mockedUrl), MockingWebConnection.MockedResponse.empty());
			} catch (MalformedURLException e) {
				throw new AssertionError(e);
			}
		});

		return mockingWebConnection;
	}

}