package ponto.gep.crawler.ahgora.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BatidaOnlineRequestDTO {
	private String identity;
	private String account;
	private String password;
	private Boolean logon;
	private BigDecimal longitude;
	private BigDecimal latitude;
	private Integer accuracy;
	
	@JsonProperty("timestamp_loc")
	private Long timestampLoc;
	
	private String provider;
	private Boolean offline;
	private Long timestamp;
	private String origin;
	private String version;
	
	@JsonProperty("identification_type")
	private String identificationType;
}
