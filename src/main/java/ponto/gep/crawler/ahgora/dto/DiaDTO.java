package ponto.gep.crawler.ahgora.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class DiaDTO {

	private LocalDate dia;
	private String horas;
	
}
