package ponto.gep.crawler.ahgora;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebRequest;

import ponto.gep.crawler.ahgora.dto.DiaDTO;
import ponto.gep.crawler.ahgora.web.client.config.WebClientFactory;

@Component
public class AhgoraConsulta {

	private static final String URL = "https://www.ahgora.com.br/api-espelho/apuracao/";

	public List<DiaDTO> getHorariosDoMes(LocalDate data) throws Exception {
		Page page = getEspelhoPonto(data);
		return parseDTO(page.getWebResponse().getContentAsString());
	}
	
	private Page getEspelhoPonto(LocalDate data) throws Exception {
		String urlMes = new StringBuilder(URL).append(data.getYear()).append("-").append(StringUtils.leftPad(String.valueOf(data.getMonthValue()), 2, "0")).toString();
		final WebRequest request = new WebRequest(new URL(urlMes), HttpMethod.GET);
		return WebClientFactory.getInstance().getPage(request);
	}
	
	private List<DiaDTO> parseDTO(String content) throws Exception {
		List<DiaDTO> dias = new ArrayList<>();
		new ObjectMapper().readTree(content).get("dias").elements()
		.forEachRemaining(diaNode -> {
			DiaDTO diaDto = new DiaDTO();
			diaDto.setDia(LocalDate.parse(diaNode.get("referencia").textValue()));
			diaNode.get("totais").iterator()
			.forEachRemaining(total -> {
				if("Horas Trabalhadas".equals(total.get("descricao").textValue())) {
					diaDto.setHoras(total.get("valor").textValue());
				}
			});
			
			if(diaDto.getHoras()!=null) {
				dias.add(diaDto);
			}
		});
		return dias;
	}
	
}
