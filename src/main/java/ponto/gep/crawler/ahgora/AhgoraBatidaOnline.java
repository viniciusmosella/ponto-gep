package ponto.gep.crawler.ahgora;

import java.math.BigDecimal;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebRequest;

import ponto.gep.crawler.ahgora.dto.BatidaOnlineRequestDTO;
import ponto.gep.crawler.ahgora.web.client.config.WebClientFactory;

@Component
public class AhgoraBatidaOnline {

	private static final String URL = "https://www.ahgora.com.br/batidaonline/verifyIdentification";

	@Value("${ahgora.matricula}") private String matricula;
	@Value("${ahgora.senha}") private String senha;

	@Value("${ahgora.batida.online.identity}") private String identity;
	@Value("${ahgora.batida.online.longitude}") private String longitude;
	@Value("${ahgora.batida.online.latitude}") private String latitude;
	
	@Autowired private ObjectMapper objectMapper;

	public void baterPonto() throws Exception {
		final WebRequest batePontoRequest = new WebRequest(new URL(URL), HttpMethod.POST);
		batePontoRequest.setAdditionalHeader("Content-Type", "application/json");
		batePontoRequest.setRequestBody(
				objectMapper.writeValueAsString(
						BatidaOnlineRequestDTO.builder()
						.identity(identity)
						.account(matricula)
						.password(senha)
						.logon(false)
						.longitude(new BigDecimal(longitude))
						.latitude(new BigDecimal(latitude))
						.accuracy(20)
						.timestampLoc(System.currentTimeMillis())
						.provider("network/wifi")
						.offline(false)
						.timestamp(System.currentTimeMillis())
						.origin("chr")
						.version("1.0.25")
						.identificationType("matricula_senha")
						.build()));
		Page response = WebClientFactory.getInstance().getPage(batePontoRequest);
		System.out.println(response.getWebResponse().getContentAsString());
	}

}
